class_name Car extends KinematicBody3D

@export_range(0, 10000) var max_speed: float = 100.0
@export var facing: Vector3 = Vector3.FORWARD
@export_range(0, 100) var acceleration: float = 10.0
@export_range(0, 45) var max_turn: float = 40

var _velocity: Vector3 = Vector3.ZERO
var _gravity: Vector3
var _speed: float = 0.0

func _ready():
	var gforce = ProjectSettings.get_setting("physics/3d/default_gravity")
	var gvector = ProjectSettings.get_setting("physics/3d/default_gravity_vector")
	_gravity = gforce * gvector


func _physics_process(delta):
	_velocity = (_gravity + (facing * _speed)) * delta
	var motion: Vector3 = move_and_slide(_velocity)
	_velocity = motion

# range value -1, 1
func turn(w: float) -> void:
	facing = facing.rotated(Vector3.UP, w * deg2rad(max_turn) * get_physics_process_delta_time())

func accelerate(delta: float) -> void:
	_speed += acceleration * delta
	if _speed > max_speed:
		_speed = max_speed
