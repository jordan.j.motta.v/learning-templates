class_name PlayerController extends Node

var _car: Car = null
@export var car_nodepath: NodePath

func _ready():
	_car = get_parent().get_node("car")

func _physics_process(delta):
	if Input.is_action_pressed("ui_select"):
		_car.accelerate(delta)
	
	if Input.is_action_pressed("ui_left"):
		_car.turn(-1)
	elif Input.is_action_pressed("ui_right"):
		_car.turn(1)
	else:
		_car.turn(0)
